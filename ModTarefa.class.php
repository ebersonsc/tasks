<?php
    class ModTarefa{
        private $cod_tarefa;
        private $nome_tarefa;
        private $desc_tarefa;
        private $anexo_tarefa;
        private $caminho_anexo;

        public function getCod_tarefa(){
            return $this->cod_tarefa;
        }
        public function setCod_tarefa($cod){
            $this->cod_tarefa = $cod;
        }
        public function getNome_tarefa(){
            return $this->nome_tarefa;
        }
        public function setNome_tarefa($nome){
            $this->nome_tarefa = $nome;
        }
        public function getDesc_tarefa(){
            return $this->desc_tarefa;
        }
        public function setDesc_tarefa($desc){
            $this->desc_tarefa = $desc;
        }
        public function getAnexo_tarefa(){
            return $this->anexo_tarefa;
        }
        public function setAnexo_tarefa($anexo){
            $this->anexo_tarefa = $anexo;
        }
        public function getCaminho_anexo(){
            return $this->caminho_anexo;
        }
        public function setCaminho_anexo($caminho_anexo){
            $this->caminho_anexo = $caminho_anexo;
        }
    }
?>