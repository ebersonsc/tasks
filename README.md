# README #
Repositório com o código fonte do Teste de Programação Básica em PHP, solicitado por e-mail. 
Os arquivos iniciados com Con* são as classes onde os métodos que acessam o banco de dados se encontram,
os iniciados com Mod* são as classes nas quais as variáveis são armazenadas.

O usuário para acesso é: "admin", e a senha: "123456".