<html>
	<head>
		<title></title>
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<style type="text/css">
		h4{
			margin-top:20px;
		}
		label{
			font-weight: bold;
			color: black;
		}
		body{
			color: #474a51;
		}
		a:link{
			text-decoration:underline;
		}
		</style>
	</head>
	<body>
		<div class="container">
			<?php
				session_start();
				//if ($_SESSION['login'] == "") {
				//	header('location:login.php');
				//}
				//echo $_SESSION['login'];
			?>
			<h4>Lista de tarefas:</h4>
			<a href="CadTarefa.php">Cadastrar nova tarefa</a></br>
			<?php
				include_once('ConTarefa.class.php');
				$ConTarefa = new ConTarefa;
				$array = $ConTarefa->ExibirTarefas();
				foreach($array as $item){
			?>
			<label for="Nome_tarefa"></br><strong>Nome da tarefa: </strong></label>
			<?php echo $item->getNome_tarefa()."</br>";?>
			<label for="Desc_tarefa"><strong>Descrição da Tarefa: </strong></label>
			<?php echo $item->getDesc_tarefa()."</br>";?>
			<label for="Anexo"><strong>Anexo: </strong></label>
			<a target="_blank" href="<?php echo $item->getCaminho_anexo(); ?>"><?php echo $item->getAnexo_tarefa();?></a></br>
			<a href="EditTarefa.php?id=<?php echo $item->getCod_tarefa(); ?>">Editar</a>
			<a href="Excluir.php?id=<?php echo $item->getCod_tarefa(); ?>">		Apagar</a></br>
			<?php
					echo "<hr>";
				}
			?>
			
		</div>
	</body>
</html>