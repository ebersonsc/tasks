<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="content-language" content="pt-br">

    <title>Cadastrar tarefa</title>
   
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/estilo.css" rel="stylesheet">
  </head>

  <body>

    <div class="container">
        <form enctype="multipart/form-data" class="form-save" action="" method="post">
            <h2 class="text-center">Cadastrar nova tarefa</h2>
            <label for="txtnome_tarefa">Nome da tarefa:</label>
            <input type="text" name="txtnome_tarefa" class="form-control" placeholder="Nome da tarefa" required autofocus>
            <label for="txtdesc_tarefa">Descrição da tarefa:</label>
            <input type="text" name="txtdesc_tarefa" class="form-control" placeholder="Descrição da tarefa" required>
            <label for="anexo_tarefa">Anexo:</label>
            <input name="anexo_tarefa" type="file" class="form-control" required/>
            <button class="btn btn-primary btn-block" name="btnsalvar" id="btnsalvar" type="submit">Salvar</button>
        </form>

    </div>

    <?php    
		if(isset($_REQUEST["btnsalvar"])){
			include_once("ModTarefa.class.php");
			$task = new ModTarefa;
			$task->setNome_tarefa($_POST['txtnome_tarefa']);
			$task->setDesc_tarefa($_POST['txtdesc_tarefa']);
			$task->setAnexo_tarefa(basename($_FILES['anexo_tarefa']['name']));
			$task->setCaminho_anexo('anexos/' . date('d-m-y') . $task->getAnexo_tarefa());
			include_once("ConTarefa.class.php");
			$cadastra = new ConTarefa;
			$res = $cadastra->CadastrarTarefa($task->getNome_tarefa(), $task->getDesc_tarefa(),$task->getAnexo_tarefa(), $task->getCaminho_anexo());
			if($res){
				echo "<div class='alert alert-success text-center'>
                Task cadastrada com sucesso!.</div>";
				header('location:Principal.php');
			}
			else{
				echo "<div class='alert alert-danger text-center'>
                Ocorreu um erro ao cadastrar a Task, verifique as informações e tente novamente.</div>";
			}
		}
    ?>

  </body>
</html>
