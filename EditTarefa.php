<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="content-language" content="pt-br">

    <title>Cadastrar tarefa</title>
   
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/estilo.css" rel="stylesheet">
  </head>

  <body>
	<?php
		$id = $_GET['id'];
		include_once("ConTarefa.class.php");
		$conTarefa = new ConTarefa;
		include_once("ModTarefa.class.php");
    $modTarefa = $conTarefa->SelecionaTarefa($id);
    
	?>
    <div class="container">
        <form enctype="multipart/form-data" class="form-save" action="" method="post">
            <h2 class="text-center">Editar task</h2>
            <label for="txtnome_tarefa">Nome da task:</label>
            <input type="text" name="txtnome_tarefa" class="form-control" placeholder="Nome da tarefa" value="<?php echo $modTarefa->getNome_tarefa();?>" required autofocus>
            <label for="txtdesc_tarefa">Descrição da tarefa:</label>
            <input type="text" name="txtdesc_tarefa" class="form-control" placeholder="Descrição da tarefa" value="<?php echo $modTarefa->getDesc_tarefa();?>" required>
            <label for="anexo_tarefa">Anexo:</label>
            <input name="anexo_tarefa" type="file" class="form-control" value="anexo" required/>
            <button class="btn btn-primary btn-block" name="btnatualizar" id="btnatualizar" type="submit">Atualizar</button>
        </form>
    </div>

    <?php
		if(isset($_REQUEST['btnatualizar'])){
			$modTarefa = new ModTarefa;
			$modTarefa->setNome_tarefa($_POST['txtnome_tarefa']);
			$modTarefa->setDesc_tarefa($_POST['txtdesc_tarefa']);
			$modTarefa->setAnexo_tarefa(basename($_FILES['anexo_tarefa']['name']));
			$modTarefa->setCaminho_anexo('anexos/' . date('d-m-y') . $modTarefa->getAnexo_tarefa());
			$res = $conTarefa->AtualizaTarefa($id, $modTarefa->getNome_tarefa(), $modTarefa->getDesc_tarefa(),$modTarefa->getAnexo_tarefa(), $modTarefa->getCaminho_anexo());
			if($res)
				echo "<div class='alert alert-success text-center'> Task atualizada com sucesso!.</div>";
			else
				echo "<div class='alert alert-danger text-center'> Ocorreu um erro ao atualizar a Task, verifique as informações e tente novamente.</div>";
			header('location:Principal.php');
		}
    ?>
  </body>
</html>
