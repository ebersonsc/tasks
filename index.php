<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="description" content="Teste para Desenvolvedor PHP Júnior da Voxus">
    <meta name="author" content="Eberson dos Santos Cosme">
    <meta http-equiv="content-language" content="pt-br">

    <title>Login</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/estilo.css" rel="stylesheet">
  </head>

  <body>

    <div class="container">

        <form class="form-signin" action="" method="post">
            <h2 class="form-signin-heading text-center">Login</h2>
            <label for="txtemail" class="sr-only">Email</label>
            <input type="email" name="txtemail" class="form-control" placeholder="Email" required autofocus>
            <label for="txtsenha" class="sr-only">Senha</label>
            <input type="password" name="txtsenha" class="form-control" placeholder="Senha" required>
            <button class="btn btn-primary btn-block" name="btnentrar" id="btnentrar" type="submit">Entrar</button>
        </form>

    </div>

    <?php
      define('ROOT_PATH', dirname(__FILE__));    
      if(isset($_REQUEST["btnentrar"])){
        include_once("ModLogin.class.php");
        $log = new ModLogin;
        $log->setEmail($_POST['txtemail']);
        $log->setSenha(md5($_POST['txtsenha']));
        session_start();
        $_SESSION['login'] = $log->getEmail();
        include_once("ConLogin.class.php");
        $entrar = new ConLogin;
        if($entrar->Entrar($log->getEmail(), $log->getSenha())){
          header("location:Principal.php");
        }
        else{
          echo "<div class='alert alert-danger text-center'>
                Usuário ou senha incorretos.</div>";
        }
      }
    ?>

  </body>
</html>
