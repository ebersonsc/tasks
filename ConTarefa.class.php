<?php
    class ConTarefa{

        private $conexao;

		/* Metodo construtor, onde a conexão com o banco de dados é aberta a cada nova instancia da classe*/
        public function __construct(){
            include_once('Conexao.class.php');
            $this->conexao = new Conexao;
        }

		/* Função para cadastrar uma nova task */
        public function CadastrarTarefa($nomeTarefa, $descTarefa, $nomeAnexo, $caminhoAnexo){
            $query = "insert into tarefas (Nome, Descricao, Anexo, Caminho_Anexo) values ('$nomeTarefa', '$descTarefa', '$nomeAnexo', '$caminhoAnexo')";
            if(move_uploaded_file($_FILES['anexo_tarefa']['tmp_name'],$caminhoAnexo)){
                $res = mysqli_query($this->conexao->getCon(), $query) or die(mysqli_error($this->conexao->getCon()));
                return true;
            }
            else{
                return false;
            }
        }
		
		/* Função para listar todas as task */
        public function ExibirTarefas(){
            include_once('ModTarefa.class.php');
            if(isset($tarefas)){ #Verifica se variavel $tarefas possue valor, caso sim a remove da memória
                unset($tarefas);
            }
            $tarefas = array();#array onde as linhas da tabela serão armazenadas
            $query = "select cod, nome, descricao, anexo, caminho_anexo from tarefas";
			#Executando a query
            $res = mysqli_query($this->conexao->getCon(), $query) or die(mysqli_error($this->conexao->getCon()));
            if(mysqli_num_rows($res) > 0){
                while($linha = mysqli_fetch_array($res)){
		            $modTarefa = new ModTarefa; #A cada linha da tabela um novo objeto é instanciado
                    $modTarefa->setCod_tarefa($linha["cod"]);
                    $modTarefa->setNome_tarefa($linha["nome"]);
                    $modTarefa->setDesc_tarefa($linha["descricao"]);
                    $modTarefa->setAnexo_tarefa($linha["anexo"]);
                    $modTarefa->setCaminho_anexo($linha["caminho_anexo"]);
                    array_push($tarefas, $modTarefa); #Adicionando o Objeto no fim do array
                }
            }
            return $tarefas;
        }
		
		/* Função para selecionar as informações de uma taks para edição*/
		public function SelecionaTarefa($id){
            include_once('ModTarefa.class.php');
            $query = "select cod, nome, descricao, anexo, caminho_anexo from tarefas where cod=$id";
			$modTarefa = new ModTarefa; #Instancia do objeto para armazenar as informações da Task
			#Executando a query
            $res = mysqli_query($this->conexao->getCon(), $query) or die(mysqli_error($this->conexao->getCon()));
            if(mysqli_num_rows($res) > 0){
                while($linha = mysqli_fetch_array($res)){
                    $modTarefa->setCod_tarefa($linha["cod"]);
                    $modTarefa->setNome_tarefa($linha["nome"]);
                    $modTarefa->setDesc_tarefa($linha["descricao"]);
                    $modTarefa->setAnexo_tarefa($linha["anexo"]);
                    $modTarefa->setCaminho_anexo($linha["caminho_anexo"]);
                }
            }
            return $modTarefa;
        }
		
		/* Função para atualizar informações da task no banco de dados */
		public function AtualizaTarefa($id, $nomeTarefa, $descTarefa, $anexo, $caminhoAnexo){
            $query = "update tarefas set Nome='$nomeTarefa', Descricao='$descTarefa',
            Anexo='$anexo', Caminho_Anexo='$caminhoAnexo' where cod=$id";
			if(move_uploaded_file($_FILES['anexo_tarefa']['tmp_name'],$caminhoAnexo)){
                $r = mysqli_query($this->conexao->getCon(), $query) or die(mysqli_error());
                return true;
            }
            else{
                return false;
            }
		}
		
		/* Função para excluir uma task */
        public function ExcluirTarefa($id){
            $query = "delete from tarefas where cod=$id";
            $res = mysqli_query($this->conexao->getCon(), $query) or die(mysqli_error());
        }
    }
?>
